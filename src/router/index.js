import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import store from '../vuex'

Vue.use(VueRouter)

const router = new VueRouter(
  {
    routes,
    linkActiveClass: 'active'
  }
)

const isAuthRoute = route => route.path.indexOf('/auth') !== -1
const isLogged = () => store.getters.isLogged

router.beforeEach((to, from, next) => {
  if (!isLogged() && !isAuthRoute(to)) {
    window.console.log('Not authenticated')
    next('/auth')
  } else {
    next()
  }
})

export default router
