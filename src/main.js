import Vue from 'vue'
import App from './app/Main.vue'
import router from './router'
import store from './vuex'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import ptBr from 'element-ui/lib/locale/lang/pt-br'
import locale from 'element-ui/lib/locale'

import * as TYPES from './vuex/types'

Vue.use(ElementUI)
locale.use(ptBr)

// Check local storage to handle refreshes
if (window.localStorage) {
  let user = JSON.parse(window.localStorage.getItem('user') || 'null')
  let token = window.localStorage.getItem('token') || ''

  if (user && store.state.user !== user) {
    store.commit(TYPES.AUTH_USER, user)
    store.commit(TYPES.AUTH_TOKEN, token)
  }
}

/* eslint-disable no-new */
new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
})
