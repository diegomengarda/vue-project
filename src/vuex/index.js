import Vue from 'vue'
import Vuex from 'vuex'
import { vuex as app } from '../app'
import * as TYPES from './types'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    listOriginalData: [],
    listData: [],
    listTotal: 0,
    listPerPage: 5,
    listCurrentPage: 1
  },
  mutations: {
    [TYPES.LIST_ORIGINAL_DATA] (state, payload) {
      state.listOriginalData = payload
    },
    [TYPES.LIST_DATA] (state, payload) {
      state.listData = payload
    },
    [TYPES.LIST_TOTAL] (state, payload) {
      state.listTotal = payload
    },
    [TYPES.LIST_PER_PAGE] (state, payload) {
      state.listPerPage = payload
    },
    [TYPES.LIST_CURRENT_PAGE] (state, payload) {
      state.listCurrentPage = payload
    }
  },
  actions: {
    setListOriginalData: (context, payload) => {
      context.commit(TYPES.LIST_ORIGINAL_DATA, payload)
      context.commit(TYPES.LIST_DATA, payload)
      context.commit(TYPES.LIST_TOTAL, payload.length)
    },
    setListData: (context, payload) => {
      context.commit(TYPES.LIST_DATA, payload)
    },
    setListPerPage: (context, payload) => {
      context.commit(TYPES.LIST_PER_PAGE, payload)
    },
    setListCurrentPage: (context, payload) => {
      context.commit(TYPES.LIST_CURRENT_PAGE, payload)
    }
  },
  getters: {
    listOriginalData: ({listOriginalData}) => listOriginalData,
    listData: ({listData}) => listData,
    listTotal: ({listTotal}) => listTotal,
    listPerPage: ({listPerPage}) => listPerPage,
    listCurrentPage: ({listCurrentPage}) => listCurrentPage
  },
  modules: {
    ...app
  }
})
