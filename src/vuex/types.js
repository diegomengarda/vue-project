export const AUTH_USER = 'AUTH_USER'
export const AUTH_TOKEN = 'AUTH_TOKEN'

export const LIST_ORIGINAL_DATA = 'LIST_ORIGINAL_DATA'
export const LIST_DATA = 'LIST_DATA'
export const LIST_TOTAL = 'LIST_TOTAL'
export const LIST_PER_PAGE = 'LIST_PER_PAGE'
export const LIST_CURRENT_PAGE = 'LIST_CURRENT_PAGE'
