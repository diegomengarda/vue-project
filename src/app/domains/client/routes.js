import List from './components/index/List'
import Form from './components/Form'

export default [
  {path: '/client', component: List, name: 'client.index'},
  {path: '/client/create', component: Form, name: 'client.create'},
  {path: '/client/:id', component: Form, name: 'client.edit'}
]
