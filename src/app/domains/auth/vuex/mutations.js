import * as TYPES from '../../../../vuex/types'

export default {
  [TYPES.AUTH_USER] (state, payload) {
    state.user = payload
  },
  [TYPES.AUTH_TOKEN] (state, payload) {
    state.token = payload
  }
}
