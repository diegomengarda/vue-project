import * as restfulService from '../../../services/restfulService'
import * as TYPES from '../../../../vuex/types'

export const login = ({commit}, payload) => {
  return restfulService.get('http://uinames.com/api/?ext')
    .then((response) => {
      console.log(response)
      window.localStorage.setItem('user', JSON.stringify(response))
      window.localStorage.setItem('token', response.email)
      commit(TYPES.AUTH_USER, response)
      commit(TYPES.AUTH_TOKEN, response.email)
    })
}

export const logout = ({commit}, payload) => {
  return new Promise(function (resolve, reject) {
    window.localStorage.removeItem('user')
    window.localStorage.removeItem('token')
    commit(TYPES.AUTH_USER, {})
    commit(TYPES.AUTH_TOKEN, '')
  })
}
