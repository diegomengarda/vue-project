import * as HTTP from '../http'

function _all (domain) {
  return HTTP.get(domain)
    .then(response => response.data)
}

function _get (domain, id) {
  let url = domain
  if (id !== undefined && id !== null) {
    url += '/' + id
  }
  return HTTP.get(url)
    .then(response => response.data)
}

function _post (domain, id, data) {
  return HTTP.post(domain, data)
    .then(response => response.data)
}

function _put (domain, id, data) {
  return HTTP.put(domain + '/' + (id || data.id), data)
    .then(response => response.data)
}

function _save (domain, id, data) {
  if (data.id !== undefined && data.id !== null) {
    return _put(domain, id, data)
  }
  return _post(domain, id, data)
}

function _destroy (domain, id) {
  return HTTP.destroy(domain + '/' + id)
    .then(response => response.data)
}

export const all = _all
export const get = _get
export const post = _post
export const put = _put
export const save = _save
export const destroy = _destroy
