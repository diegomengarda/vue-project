import axios from 'axios'
import * as HTTP from '../http'

export default {
  get (domains) {
    const promises = []
    const responses = {}

    if (domains !== undefined && domains.length > 0) {
      domains.forEach(function (val) {
        if (val !== undefined) {
          // let data = (val.data !== false ? val.data : null)
          switch (val.domain) {

            case 'client_categories':
              promises.push(HTTP.get('client_category'))
              responses.client_categories = []
              break

          }
        }
      })
    }

    let i = 0
    return axios.all(promises)
      .then((result) => {
        for (let index in responses) {
          if (result[i] !== undefined) {
            responses[index] = result[i].data
          } else {
            responses[index] = []
          }
          i++
        }
        return responses
      })
  }
}
