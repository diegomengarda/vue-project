import { routes as auth } from './domains/auth'
import { routes as dashboard } from './domains/dashboard'
import { routes as client } from './domains/client'

export default [...auth, ...dashboard, ...client]
