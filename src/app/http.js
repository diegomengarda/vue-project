import axios from 'axios'
import { isEmpty } from 'lodash'

const token = window.localStorage.getItem('token') || ''
const baseAPIURL = 'http://localhost:3000/'
const APIVersion = ''

const axiosConfig = {
  baseURL: (baseAPIURL + APIVersion),
  headers: {
    'content-type': 'application/json'
  }
}

if (!isEmpty(token)) {
  axiosConfig.headers.Authorization = 'Bearer ' + token
}

const _http = axios.create(axiosConfig)

function _get (url) {
  return _http.get(url)
}

function _post (url, data) {
  return _http.post(url, data)
}

function _put (url, data) {
  return _http.put(url, data)
}

function _destroy (url) {
  return _http.delete(url)
}

export const get = _get
export const post = _post
export const put = _put
export const destroy = _destroy
